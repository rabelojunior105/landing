import React from 'react';

const Features = () => {
    return (
        <section className="service-one" id="features">
            <div className="container">
                <div className="block-title text-center">
                    <h2 className="block-title__title">
                        Você pode ser nosso parceiro <br /> das <span> seguintes</span> formas
                        </h2>
                </div>
                <div className="row">
                    <div className="col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-duration="1500ms">
                        <div className="service-one__single text-center">
                            <div className="service-one__inner">
                                <i className="service-one__icon dimon-icon-target"></i>
                                <h3><a href="#">Representantes</a></h3>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-sm-12 wow fadeInDown" data-wow-duration="1500ms">
                        <div className="service-one__single text-center">
                            <div className="service-one__inner">
                                <i className="service-one__icon dimon-icon-presentation"></i>
                                <h3><a href="#">Afiliados</a></h3>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-duration="1500ms">
                        <div className="service-one__single text-center">
                            <div className="service-one__inner">
                                <i className="service-one__icon dimon-icon-laptop"></i>
                                <h3><a href="#">Lojas</a></h3>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    )
}
export default Features;