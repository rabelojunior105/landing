import React from 'react';
import { Form, Input } from '@rocketseat/unform';
import { toast } from 'react-toastify'
import axios from 'axios'

export default function Banner() {
    async function handleSubmit(data, { resetForm }) {
        await axios.post('http://api.portaldamaria.com.br/users', {
            "name": data.name,
            "company_name": data.company_name,
            "cnpj": data.cnpj,
            "tellphone": data.tellphone,
            "email": data.email,
            "password": data.password
        })
            .then(function (response) {
                toast.success('Cadastrado com successo!');
                resetForm();

            }).catch(function (err) {
                toast.error('Cadastro não pode ser efetuado, por favor tente novamente!');
                resetForm();
                console.log(err.response)
            })
    };


    return (
        <section className="banner-one" id="banner" >

            <span className="banner-one__shape-1"></span>
            <span className="banner-one__shape-2"></span>
            <span className="banner-one__shape-3"></span>
            <span className="banner-one__shape-4"></span>
            <div className="container">
                <div className="row">
                    <div className="col-xl-6 col-lg-8">
                        <div className="banner-one__content">
                            <h3 className="banner-one__title"> <span>Quer vender mais ?</span>
                                <br />Vem para o <br /> portal da maria.</h3>
                            <p className="banner-one__text">
                                Se inscreva no maior Marketplace para sua empresa!
                                O melhor parceiro que você poderá ter para o seu negócio, comprar, vender, intermediar.... Vem para o Portal da Maria!
                            </p>
                        </div>
                    </div>
                    <div className="col-xl-6 col-lg-8">
                        <div className="banner-one__moc">
                        </div>
                        <div className="accrodion-grp faq-accrodion">
                            <div className="accrodion active">
                                <div className="accrodion-inner">
                                    <div className="accrodion-title text-center">
                                        <h2 >Pré Cadastro</h2>
                                    </div>
                                    <div className="accrodion-content mt-5">
                                        <Form onSubmit={handleSubmit} method="post" className="reply-form">
                                            <div className="row m-2 pt-3">
                                                <div className="col-lg-12">
                                                    <Input
                                                        type="text"
                                                        placeholder="Digite o seu nome completo"
                                                        className="reply-form__field"
                                                        name="name"
                                                    />

                                                </div>
                                                <div className="col-lg-12">
                                                    <Input
                                                        type="text"
                                                        placeholder="Digite o nome da empresa"
                                                        name="company_name"
                                                        className="reply-form__field"

                                                    />
                                                </div>

                                                <div className="col-lg-6">
                                                    <Input
                                                        type="text"
                                                        placeholder="Digite o telefone comercial"
                                                        name="tellphone"
                                                        className="reply-form__field"

                                                    />
                                                </div>
                                                <div className="col-lg-6">
                                                    <Input
                                                        type="text"
                                                        placeholder="Digite o cnpj da empresa"
                                                        name="cnpj"
                                                        className="reply-form__field"


                                                    />
                                                </div>

                                                <div className="col-lg-12">
                                                    <Input
                                                        type="email"
                                                        placeholder="Digite seu email"
                                                        className="reply-form__field"
                                                        name="email"


                                                    />
                                                </div>

                                                <div className="col-lg-12">
                                                    <Input
                                                        type="password"
                                                        name="password"
                                                        placeholder="Digite sua senha"
                                                        className="reply-form__field"


                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <button className="reply-form__btn thm-btn" type="submit">
                                                        <span>Pré Cadastrar</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </Form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section >
    )
}
