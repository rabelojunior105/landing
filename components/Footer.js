import React, { Component } from 'react';

export default class Blog extends Component {
    constructor() {
        super()
        this.state = {
            scrollBtn: false
        }
        this.handleScroll = this.handleScroll.bind(this)
        this.scrollTop = this.scrollTop.bind(this)
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll() {

        if (window.scrollY > 70) {
            this.setState({
                scrollBtn: true
            });
        } else if (window.scrollY < 70) {
            this.setState({
                scrollBtn: false
            });
        }

    }

    scrollTop = () => {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div>
                <footer className="site-footer">
                    <div className="site-footer__upper">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-3">
                                    <div className="footer-widget footer-widget__about">
                                        <img src="/assets/images/resources/logo.png" width="300" alt=""
                                            className="footer-widget__logo" />
                                        <p className="footer-widget__contact"><a href="tel:888-666-0000">
                                            +55 11 94230 8683
                                            </a>
                                        </p>

                                        <p className="footer-widget__contact">
                                            <a href="mailto:gerencia@fhibra.com">gerencia@fhibra.com</a>
                                        </p>
                                        <p className="footer-widget__contact">
                                            <a href="mailto:gerencia@fhibra.com">gerencia@fhibra.com</a>
                                        </p>
                                        <p className="footer-widget__contact">
                                            <a href="mailto:gerencia@fhibra.com">R.Dr.Virgíllio do Nascimento, 445 <br />
                                             Brás, São Paulo - SP,<br /> 03027-020,Brasil
                                             </a>
                                        </p>

                                    </div>
                                </div>
                                <div className="col-lg-6 d-flex justify-content-between footer-widget__links-wrap"></div>
                                <div className="col-lg-6 pt-5">
                                    <div className="footer-widget">
                                        <div className="site-footer__social">
                                            <a href="#" className="fa fa-facebook-square"></a>
                                            <a href="#" className="fa fa-instagram"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="site-footer__bottom">
                        <div className="container">
                            <div className="inner-container text-center">
                                <p className="site-footer__copy">&copy; copyright 2020 by <a href="https://www.creativedd.com.br">CREATIVEDD.COM.BR</a></p>
                            </div>
                        </div>
                    </div>
                </footer >

                <div onClick={this.scrollTop} className="scroll-to-target scroll-to-top" style={{ display: this.state.scrollBtn ? 'block' : 'none' }}><i className="fa fa-angle-up"></i></div>

            </div >
        )
    }
}