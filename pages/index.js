
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Layout from "../components/Layout";
import NavOne from "../components/NavOne";
import Footer from "../components/Footer";
import Banner from "../components/Banner";
import Features from "../components/Features";


const HomePage = () => (

    <Layout pageTitle="Portal da Maria - Pré Cadastro">
        <NavOne />
        <ToastContainer autoClose={3000} />
        <Banner />
        <Features />
        <Footer />
    </Layout>

)

export default HomePage;